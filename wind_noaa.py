#!/usr/bin/python3
import datetime, time, os, os.path, string
import numpy as np
from subprocess import call
import matplotlib
matplotlib.use('QT5Agg')
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.spatial as spatial
from operator import add
import matplotlib as mpl
from matplotlib import cm
from matplotlib import rc
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
import wind_noaa_functions as wndl
from datetime import datetime, timedelta, timezone
import calendar

print("\n##########################################################")
print("###                                                    ###")
print("###       WELCOME TO NOAA DATA VISUALIZER              ###")
print("###                                                    ###")
print("###       by Sven Schmid, Elektra Solar GmbH           ###")
print("##########################################################\n")


filename = input("Enter Filename, including .txt\n")

time_shift_to_zulu_time = input("\nHow many hours ahead of Zulu-Time?\nEnter positive integer for location east of London, E.g. Pingyuanzhen: 8 \n")
location_altitude = float(input("\nEnter the Airfield Altitude AMSL [m]. E.g. Pingyuanzhen: 1500\n"))

### READ HEADER ###
timedeltas_str= wndl.readHeader(filename)[4]
timedeltas_float=[]
for i in range(0,len(timedeltas_str)):
	timedeltas_float.append(float(timedeltas_str[i]))

location = wndl.readHeader(filename)[0][0:-1]
start_hour=wndl.readHeader(filename)[1][31:33]
start_month = wndl.GetMonth(wndl.readHeader(filename)[1][22:25])
start_day=wndl.readHeader(filename)[1][19:21]
start_year=wndl.readHeader(filename)[1][28:30]

#print(timedeltas_float)
#print(location)
#print(start_hour,start_day,start_month,start_year)

timestring=(str(start_day) + '/' + str(start_month) + '/'+ str(start_year) + ' ' +str(start_hour) + ':' +str(00))
#print(timestring)

start_time =datetime.strptime(timestring, '%d/%m/%y %H:%M')
#print(start_time)

delta_hours=45
#print(delta_hours)
#print(start_time + timedelta(hours=delta_hours+int(time_shift_to_zulu_time)))

local_time=[]
zulu_time=[]




	
### READ WIND DATA ### getWindgramData
windgramdata = wndl.getWindgramData(filename)


### LOOP OVER ALL TIMEDELTAS ###
wdata=[]
for i in range(0,len(timedeltas_float)):
	local_time.append(start_time + timedelta(hours=timedeltas_float[i] + int(time_shift_to_zulu_time) ))
	zulu_time.append(start_time + timedelta(hours=timedeltas_float[i]))
	#print("Timedelta =",timedeltas_float[i]," h")
	wdata.append(wndl.getPresDirSpeedVec(windgramdata, timedeltas_float[i]))
	#pressures, directions, speeds, altitudes = wndl.getPresDirSpeedVec(windgramdata, timedeltas_float[i])
	#for idx, val in enumerate(pressures):
		#print("Pascal:",pressures[idx],"degrees:",directions[idx],"m/s:",speeds[idx],"Altitude:",altitudes[idx],"m")
	#print()



### CONVERT ALL ENTRIES OF wdata TO FLOAT ###
wdata_float = np.array(wdata,dtype=float)

#print(wdata_float[0][3][:])

### PRINT ALL FIGURES ###
wndl.allFigsPrint(wdata_float,timedeltas_float,local_time, location,location_altitude,time_shift_to_zulu_time,filename)

### WRITE ALL DATA TO CSV-FILE ###
wndl.CSVWriteAllFigs(wdata_float,timedeltas_float,local_time, location,location_altitude,time_shift_to_zulu_time,filename)


selected_time=1
while selected_time != 'c':
	selected_time = int(input("Which number to plot? (1,2,3....). Enter c to close application...\n"))
	### PRINT THE FIRST FIGURE ###
	wndl.fig1Print(wdata_float,int(selected_time),local_time[int(selected_time)], location,location_altitude,zulu_time[int(selected_time)],filename)

plt.show(block=False)


