#!/usr/bin/python3
import datetime, time, os, os.path
import numpy as np 
from subprocess import call 
from numpy.linalg import inv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from decimal import Decimal

#                       
# ## example_windgram.txt shown for convenience below (# not in file):
#
#hysplit.t06z.gfsf                                         
# Latitude: 48.08 Longitude:  11.28 &               
# DATA INITIAL TIME: 29 MAR 2018 06Z&
# CALCULATION STARTED AT: 29 MAR 2018 06Z&
# HOURS OF CALCULATION:           84 &
#
#           WIND DIRECTION @ WIND SPEED (DEG/KNOTS)
#FHR:      +  0.   +  3.   +  6.   +  9.   + 12.   + 15.   + 18.   + 21.   + 24.   + 27.   + 30.   + 33.   + 36.   + 39.   + 42.   + 45.   + 48.   + 51.   + 54.   + 57.   + 60.   + 63.   + 66.   + 69.   + 72.   + 75.   + 78.   + 81.   + 84.
#  
#  20.mb  67@017  76@020  87@018  81@015  79@016  77@016  73@017  78@018  84@018  90@015  88@012  76@011  72@013  79@012  81@010  55@009  57@012  75@010  78@008  49@005  43@007  38@007  41@008  39@007  42@005  52@004  55@001 336@005   0@004
#  50.mb 291@010 281@009 265@008 253@010 264@012 266@010 248@009 242@012 249@013 248@011 242@011 240@013 250@014 257@013 261@012 260@010 257@011 258@013 265@013 272@014 283@013 271@011 269@015 281@016 277@013 272@015 285@017 286@011 262@012
# 100.mb 268@026 262@026 254@027 252@029 250@030 248@031 245@031 242@031 240@030 236@031 236@033 240@033 241@029 237@026 235@026 241@026 245@023 245@021 245@022 253@022 259@020 266@020 268@021 275@023 283@023 278@021 273@024 280@027 287@027
# 150.mb 258@040 253@039 253@040 251@042 251@044 248@043 246@042 244@043 243@045 238@045 235@046 236@045 235@043 229@043 225@040 226@037 233@032 231@026 228@026 236@026 252@022 257@019 263@019 272@025 284@028 288@029 287@031 287@034 287@036
# 200.mb 257@053 252@051 248@056 250@060 254@059 257@061 255@063 249@060 243@059 240@060 234@063 233@062 230@058 225@053 221@043 214@036 209@034 214@033 220@029 222@023 230@020 242@018 267@018 272@021 280@026 289@030 292@035 298@041 307@047
# 250.mb 256@074 250@064 246@065 250@068 261@074 264@076 261@076 250@067 242@061 238@065 234@075 234@074 227@081 216@077 207@067 203@045 203@035 199@030 199@031 201@025 205@018 223@015 254@016 279@017 276@020 280@025 296@031 304@041 315@058
# 300.mb 256@078 253@064 248@062 249@065 267@081 264@077 263@076 251@069 243@062 241@062 236@073 237@077 229@075 219@070 205@061 195@046 187@032 182@020 176@018 190@015 193@010 202@008 238@014 255@022 253@021 262@017 287@023 313@028 322@047
# 350.mb 259@063 255@057 249@058 244@061 259@068 255@067 257@068 252@066 244@061 238@061 234@065 235@065 231@050 224@047 207@043 193@037 173@015  93@011 105@014 194@013 197@017 205@012 224@018 233@024 230@030 247@023 287@018 340@014 344@024
# 400.mb 255@049 254@049 248@055 241@059 247@060 251@058 251@058 253@058 247@056 238@058 234@058 235@056 230@043 219@038 207@037 194@036 183@013 146@005 152@010 207@017 209@008 212@008 235@013 242@021 244@023 251@023 282@026 314@019 351@023
# 450.mb 248@043 251@044 247@051 239@054 236@051 246@049 245@048 249@049 246@049 239@051 237@049 236@051 228@044 217@037 207@038 196@030 192@014 191@009 193@014 217@014 254@005 280@005 272@011 268@016 274@017 260@019 277@027 291@028 330@026
# 500.mb 245@041 246@042 240@046 235@047 228@042 234@040 232@041 239@041 241@039 240@039 239@040 238@044 227@049 216@038 206@042 199@026 192@014 197@011 211@016 236@012 282@006 307@009 296@012 296@017 297@024 278@021 273@027 293@028 314@030
# 550.mb 245@043 236@040 225@038 230@040 225@036 224@033 222@035 223@035 234@031 235@029 239@031 239@038 225@049 215@039 198@042 202@024 196@013 197@011 217@015 258@009 304@008 316@012 317@018 311@021 298@026 281@025 273@027 291@029 307@030
# 600.mb 249@047 230@038 218@032 222@035 233@031 230@029 220@027 218@030 225@028 229@027 234@036 238@036 220@040 210@036 186@037 195@022 200@010 202@009 213@011 273@008 296@014 323@020 324@022 312@024 295@028 280@027 276@028 289@030 303@029
# 650.mb 254@046 240@038 233@029 233@029 252@032 251@029 240@021 226@021 217@021 215@020 222@033 230@039 219@024 200@032 180@032 186@017 219@004 220@008 223@009 278@011 292@019 322@022 317@023 307@026 289@031 280@030 279@030 289@031 305@029
# 700.mb 258@042 256@040 258@032 262@028 270@030 271@028 272@022 261@017 221@012 192@013 210@023 221@032 223@016 197@029 179@019 191@009 272@006 253@010 252@010 283@015 301@020 314@021 304@023 297@028 286@032 282@033 281@032 291@030 304@028
# 750.mb 265@038 267@034 273@026 278@021 285@019 285@015 284@015 282@012 257@005 150@010 188@018 209@023 206@014 201@018 226@006 272@005 289@011 276@013 280@011 301@015 307@018 304@021 289@025 288@029 284@032 281@035 281@034 289@032 301@028
# 800.mb 269@031 267@025 271@015 276@013 276@007 266@004 262@005 246@005 149@002 116@013 158@016 175@017 177@015 219@013 300@013 300@014 301@016 295@014 301@012 312@015 312@020 298@021 285@027 285@030 284@033 271@037 269@038 280@036 295@030
# 850.mb 268@026 268@016 271@010 268@007 226@004 206@005 193@006 180@007 134@010 113@018 117@016 134@015 153@018 249@020 286@020 295@022 304@019 302@014 306@011 312@011 312@019 294@024 283@031 279@034 276@035 266@036 265@037 274@035 287@032
# 900.mb 281@019 276@011 273@008 269@005 188@003 171@007 177@009 167@012 146@013 114@013 106@015 123@013 126@017 241@017 262@018 272@018 286@014 290@011 307@009 308@009 308@016 273@023 263@027 258@030 263@028 263@031 263@033 272@031 282@029
# 925.mb 285@013 279@009 275@007 270@004 166@003 166@007 179@009 169@008 148@009 111@010 104@012 117@010 116@010 228@010 259@011 268@011 283@009 288@008 302@007 309@008 293@008 260@013 258@017 255@019 261@020 261@025 263@027 271@026 281@023
# 950.mb 284@007 281@007 275@006 270@003 164@003 166@004 176@005 165@005 143@004 110@008 102@011 116@009 111@007 243@009 254@009 267@009 281@007 289@007 304@006 311@007 284@005 255@010 254@011 253@013 260@014 260@018 263@021 271@018 277@013
# 975.mb 284@007 281@007 276@005 270@003 164@003 165@004 176@005 165@005 143@004 110@008 102@011 116@009 111@007 243@009 253@009 267@009 281@007 289@007 304@006 311@007 284@005 255@010 254@011 253@013 260@014 260@018 263@021 271@018 277@013
#1000.mb 284@007 281@007 276@005 270@003 164@003 165@004 176@005 165@004 143@004 110@008 102@011 116@009 111@007 240@009 255@009 267@009 281@007 289@007 306@006 311@007 284@005 254@010 254@011 252@014 260@014 260@018 262@021 271@018 277@013


#%%
import numpy as np
import scipy.interpolate as sp
import math



def readHeader(filename):
    fil_con=open(filename) 
    dummy=fil_con.readline()
    place=str.strip(fil_con.readline())
    initial_time=str.strip(fil_con.readline())
    calculation_start=str.strip(fil_con.readline())
    duration=str.strip(fil_con.readline())
    dummy=fil_con.readline()
    dummy=fil_con.readline()
    timedeltas=str.split(str.strip(fil_con.readline())[12:],'+')
   
    return place,initial_time,calculation_start,duration,timedeltas



#%%
#getWindgramData imports noaa data from windgram txt file as list of lines starting at line 10
#each line is split in the colums of the file
def getWindgramData(filename):
    with open(filename) as fin:
        lines = []
        line_count = 0
        for line in fin:
            line_count += 1
            if line_count >= 10:
                lines.append(line.split())
    return lines





#%%
#getPresDirSpeedVec [Pa, deg (0-360deg), km/h]  all as lists
#wndgrmdata [from getWindgramData]
#relativetime[h] data column according Line 8 windgram textdata "FHR", 0 = now, 3 = 3hours into the future
#-> NOTE: no time interpolation, choose correct time
def getPresDirSpeedVec(wndgrmdata, relativetime):
    pressures = []
    directions = []
    speeds = []
    altitudes = []
    timecolumn = int(round(relativetime / 3))
    for n in wndgrmdata:
        press = int(n[0].split('.mb')[0]) * 100.
        pressures.append(press)
        dir_speed = n[timecolumn + 1].split('@')
        directions.append(dir_speed[0])
        speeds.append(int(dir_speed[1]) * 1.852)
        altitudes.append(getPressureAltISA(press))
    

    return pressures, directions, speeds, altitudes



#%%
#getISAPresDensTemp calculates pressure, density, temperature according to ISA for altitude 0 to 47000m
#returns:
#pressure [Pa]
#density [kg/cbm]
#temperature [K]
#input:
#altitude [m] (0 - 47000)
#https://gist.github.com/buzzerrookie/5b6438c603eabf13d07e
def isa_cal(p0, t0, a, h0, h1, g, R):    
    if a != 0:
        t1 = t0 + a * (h1 - h0)
        p1 = p0 * (t1 / t0) ** (-g / a / R)
    else:
        t1 = t0
        p1 = p0 * math.exp(-g / R / t0 * (h1 - h0))
    return t1, p1

def getISAPresDensTemp(altitude):    
    g = 9.80665
    R = 287.00
    a = [-0.0065, 0, 0.001, 0.0028]
    h = [11000, 20000, 32000, 47000]
    p0 = 101325
    t0 = 288.15
    prevh = 0
    if altitude < 0 or altitude > 47000:
        print("altitude must be in [0, 47000]")
        return
    for i in range(0, 4):
        if altitude <= h[i]:
            temperature, pressure = isa_cal(p0, t0, a[i], prevh, altitude, g, R)
            break;
        else:
            # sth like dynamic programming
            t0, p0 = isa_cal(p0, t0, a[i], prevh, h[i], g, R)
            prevh = h[i]

    density = pressure / (R * temperature)

    return pressure, density, temperature



#%%
#getPressureAltISA[m] returns the corresponding altitude to a given pressure under ISA conditions 
#-> the so called pressure altitude
#pressure[Pa]
#Notes:
#isa_pressures list obtained as follows:
#altitude = range(0,47000,100)
#pressures = []
#for n in altitude:
#    pressure, density, temperature = getISAPresDensTemp(n)
#    pressures.append(pressure)
#print(pressures)
def getPressureAltISA(pressure):    
    
    isa_pressures = [101325.0, 100129.21880180092, 98944.89080166376, 97771.93188126804, 96610.25835137522, 95459.7869506081, 94320.4348442293, 93192.11962292092, 92074.75930156556, 90968.27231802691, 89872.57753193217, 88787.59422345406, 87713.24209209466, 86649.44125546947, 85596.11224809168, 84553.17602015838, 83520.55393633626, 82498.16777454855, 81485.9397247634, 80483.79238778121, 79491.6487740248, 78509.43230232858, 77537.06679872936, 76574.47649525794, 75621.58602873077, 74678.32043954318, 73744.60517046244, 72820.36606542241, 71905.52936831868, 71000.02172180373, 70103.77016608418, 69216.70213771757, 68338.74546841033, 67469.82838381696, 66609.87950233904, 65758.82783392585, 64916.60277887499, 64083.13412663441, 63258.35205460498, 62442.18712694324, 61634.57029336624, 60835.43288795544, 60044.70662796266, 59262.32361261669, 58488.21632192951, 57722.317615504944, 56964.560731346755, 56214.87928466798, 55473.20726670138, 54739.47904350996, 54013.62935479895, 53295.59331272776, 52585.306400723784, 51882.70447229587, 51187.72374984919, 50500.30082350103, 49820.37264989674, 49147.87655102698, 48482.7502130457, 47824.93168508847, 47174.359378092224, 46530.972063615416, 45894.70887265889, 45265.50929448782, 44643.31317545426, 44028.06071782052, 43419.69247858332, 42818.14936829888, 42223.37264990858, 41635.30393756545, 41053.88519546184, 40479.058736657265, 39910.76722190761, 39348.953658494785, 38793.56139905738, 38244.534140422016, 37701.81592243567, 37165.35112679853, 36635.08447589793, 36110.961031643004, 35592.92619430011, 35080.92570132915, 34574.90562622064, 34074.81237733376, 33580.59269673484, 33092.193659037206, 32609.56267024151, 32132.647466576876, 31661.39611334302, 31195.757003753177, 30735.678857777755, 30281.110720988952, 29832.0019634062, 29388.30227834231, 28949.961681250523, 28516.930508572626, 28089.15941658753, 27666.599380260952, 27249.201692095943, 26836.917960984032, 26429.700111057544, 26027.500380542624, 25630.27132061293, 25237.965794244596, 24850.536975071656, 24467.93834624255, 24090.12369927744, 23717.047132926364, 23348.66305202836, 22984.926166371206, 22625.79148955242, 22271.742325220825, 21923.233334404125, 21580.177824276387, 21242.490458583998, 20910.087236417985, 20582.885471318503, 20260.80377070625, 19943.76201563584, 19631.68134086587, 19324.484115240986, 19022.093922380795, 18724.435541671075, 18431.43492955235, 18143.01920110133, 17859.116611900503, 17579.656540191474, 17304.56946930756, 17033.78697038126, 16767.24168532237, 16504.867310062375, 16246.598578061123, 15992.371244071526, 15742.122068158338, 15495.788799966993, 15253.310163238617, 15014.625840567347, 14779.676458396161, 14548.403572247506, 14320.749652185023, 14096.658068502777, 13876.073077638426, 13658.939808306808, 13445.204247850525, 13234.813228804089, 13027.71441566836, 12823.85629189188, 12623.188147055962, 12425.660064260304, 12231.222907705967, 12039.828310472705, 11851.42866248748, 11665.977098681338, 11483.42748733153, 11303.734418586087, 11126.853193167979, 10952.73981125599, 10781.350961539634, 10612.64401044529, 10446.576991530976, 10283.108595047031, 10122.198157660188, 9963.80565233841, 9807.89167839404, 9654.417451682739, 9503.344794955792, 9354.636128363389, 9208.2544601065, 9064.163377235045, 8922.327036590037, 8782.710155887475, 8645.278004941754, 8509.996397026413, 8376.831680370056, 8245.750729785368, 8116.72093842911, 7989.710209691047, 7864.686949209798, 7741.620057013648, 7620.478919784306, 7501.233403241735, 7383.853844648159, 7268.311045429335, 7154.5762639113245, 7042.621208170882, 6932.418028997759, 6823.939312967107, 6717.158075620289, 6612.04775475241, 6508.582203804872, 6406.735685361323, 6306.4828647453905, 6207.798803718591, 6110.658954276857, 6015.039152544126, 5920.91561276151, 5828.264921370491, 5737.064031188717, 5647.290255676947, 5558.9212632956815, 5471.935071950121, 5386.329643244493, 5302.102027279685, 5219.22942817173, 5137.689437444162, 5057.460027270132, 4978.519543835447, 4900.846700820299, 4824.420572997489, 4749.220589945216, 4675.226529871792, 4602.418513550966, 4530.776998365309, 4460.282772455683, 4390.916948975505, 4322.660960446735, 4255.4965532168835, 4189.405782014511, 4124.37100460164, 4060.3748765212918, 3997.4003459383957, 3935.430648572392, 3874.4493027198378, 3814.440104365363, 3755.3871223795268, 3697.2746938015835, 3640.087419206186, 3583.8101581519395, 3528.428024710846, 3473.9263830767754, 3420.2908432515246, 3367.5072568074393, 3315.561712724821, 3264.4405333029445, 3214.130270143305, 3164.617700203801, 3115.8898219225625, 3067.9338514101646, 3020.737218708975, 2974.287564118563, 2928.572734585519, 2883.580780157307, 2839.2999504981954, 2795.71869146661, 2752.8256417527655, 2710.609629575191, 2669.0596694355195, 2628.164958930164, 2587.9148756179934, 2548.298973942971, 2509.306982210772, 2470.9287996184084, 2433.1544933359082, 2395.9742956391083, 2359.3786010926583, 2323.3579637823177, 2287.9030945956683, 2253.0048585503805, 2218.654272169151, 2184.8425009005878, 2151.560856584962, 2118.8007949643647, 2086.553913236209, 2054.811947649426, 2023.566771142549, 1992.810391022955, 1962.5349466865255, 1932.732707376997, 1903.3960699843033, 1874.5175568812056, 1846.0898137975348, 1818.1056077313706, 1790.5578248965026, 1763.4394687055117, 1736.7436577879191, 1710.4636240425814, 1684.592710723984, 1659.1243705616478, 1634.0521639120525, 1609.3697569427425, 1585.0709198476197, 1561.1495250932926, 1537.599545695677, 1514.4150535264005, 1491.590217648461, 1469.119302680636, 1446.996667190118, 1425.2167621128865, 1403.7741292013154, 1382.6633994985884, 1361.8792918393158, 1341.4166113760239, 1321.2702481310646, 1301.435175573345, 1281.9064492196371, 1262.679205259829, 1243.7486592058997, 1225.1101045640557, 1206.7589115296757, 1188.6905257046553, 1170.9004668367597, 1153.3843275805896, 1136.1377722797906, 1119.1565357701156, 1102.436422203037, 1085.9733038893744, 1069.763120162871, 1053.801876263086, 1038.0856422374256, 1022.6105518619672, 1007.3728015806415, 992.368649462601, 977.5944141773471, 963.046473987346, 948.7212657578226, 934.6152839834317, 920.7250798315112, 907.0472602016278, 893.5784868011316, 880.3154752364392, 867.2549941197711, 854.3988843482266, 841.7487187200182, 829.3009526233709, 817.0521066708675, 804.9987654218526, 793.1375761313876, 781.4652475251472, 769.9785485997223, 758.6743074477323, 747.549410107235, 736.6007994348815, 725.8254740023283, 715.220487015362, 704.7829452552611, 694.5100080419297, 684.3988862182827, 674.446841155478, 664.6511837784985, 655.0092736116612, 645.5185178436375, 636.1763704115326, 626.9803311036395, 617.927944680444, 609.0168000135172, 600.2445292418752, 591.6088069454602, 583.1073493353547, 574.737913460397, 566.4982964298163, 558.3863346515606, 550.3999030859956, 542.536914514618, 534.7953188234882, 527.1731023010517, 519.6682869500667, 512.2789298133102, 505.00312231278764, 497.83898960216743, 490.78468993213403, 483.83841402842137, 476.9983844822281, 470.2628551527745, 463.6301105817315, 457.098465419291, 450.6662638616103, 444.3318790994041, 438.0937127774591, 431.95019446482263, 425.8997811354597, 419.9409566591465, 414.0722313024045, 408.2921412392461, 402.59924807153686, 396.99213835878385, 391.46942315713534, 386.0297375674168, 380.6717402920045, 375.3941132003692, 370.19556090309203, 365.07481033419214, 360.0306103415858, 355.0617312855167, 350.16696464479554, 345.3451226306658, 340.5950378081787, 335.9155627248828, 331.3055695467114, 326.7639497008973, 322.2896135257805, 317.88148992737626, 313.5385260425482, 309.25968690866716, 305.04395513961305, 300.89033060800375, 296.7978301335091, 292.7654871771395, 288.7923515413782, 284.87748907605186, 281.0199813898084, 277.21892556709753, 273.47343389054714, 269.78263356861487, 266.14566646841996, 262.56168885364275, 259.0298711274008, 255.54939757998935, 252.11946614139515, 248.73928813849392, 245.40808805682218, 242.1251033068517, 238.8895839946593, 235.700792696916, 232.55800424010263, 229.46050548387595, 226.40759510849327, 223.39858340621961, 220.43279207664378, 217.50955402581553, 214.62821316913596, 211.7881242379255, 208.98865258959387, 206.229174021346, 203.50907458735202, 200.8277504193104, 198.18460755034297, 195.57906174215069, 193.01053831537425, 190.47847198308727, 187.9823066873683, 185.52149543888584, 183.09550015944566, 180.70379152743052, 178.3458488260918, 176.0211597946179, 173.72922048194735, 171.4695351032534, 169.24161589906595, 167.0449829969609, 164.87916427578784, 162.74369523236814, 160.6381188506324, 158.5619854731332, 156.51485267490358, 154.49628513960658, 152.50585453793295, 150.54313940820657, 148.60772503915177, 146.69920335478707, 144.81717280139512, 142.96123823654, 141.13101082008407, 139.32610790717106, 137.54615294313496, 135.79077536030277, 134.05961047664857, 132.3522993962692, 130.66848891164273, 129.00783140764173, 127.36998476725947, 125.75461227902781, 124.16138254608116, 122.58996939685021, 121.04005179734129, 119.51131376498364, 118.00344428400189, 116.51613722229916, 115.04909124981032, 113.60200975830537, 112.17460078261448]
    
    altitudes = range(0,47000,100)

    interp_lin = sp.interp1d(isa_pressures, altitudes, kind='linear', fill_value=(0, 47000), bounds_error=False)
    
    return interp_lin(pressure)/1.



#%%
#getISAAltPresDirSpeedVec [m, Pa, deg (0-360deg), km/h] all as lists
#It is a wrapper for getPresDirSpeedVec which adds corresponding isa_altitudes
#wndgrmdata [from getWindgramData]
#relativetime[h] data column according Line 8 windgram textdata "FHR", 0 = now, 3 = 3hours into the future
#-> NOTE: no time interpolation, choose correct time
def getISAAltPresDirSpeedVec(windgramdata, relativetime):
    pressures, directions, speeds = getPresDirSpeedVec(windgramdata, 0)
    isa_altitudes = []
    for n in pressures:
        isa_altitudes.append(getPressureAltISA(n))
    return isa_altitudes, pressures, directions, speeds



#%%
#getWind [deg, km/h]
#altitude [m]
#isa_altitudes, directions, speeds -> from getISAAltPresDirSpeedVec
def getWind(altitude, isa_altitudes, directions, speeds):
    interp_lin_dirs = sp.interp1d(isa_altitudes, directions, kind='linear', fill_value=(0, isa_altitudes[0]), bounds_error=False)
    interp_lin_spds = sp.interp1d(isa_altitudes, speeds, kind='linear', fill_value=(0, isa_altitudes[0]), bounds_error=False)
    wind_dir = interp_lin_dirs(altitude)/1.
    wind_speed = interp_lin_spds(altitude)/1.
    return wind_dir, wind_speed



def GetMonth(argument):
	if argument == "JAN":
		month=1
	if argument == "FEB":
		month=2
	if argument == "MAR":
		month=3
	if argument == "APR":
		month=4
	if argument == "MAY":
		month=5
	if argument == "JUN":
		month=6
	if argument == "JUL":
		month=7
	if argument == "AUG":
		month=8
	if argument == "SEP":
		month=9
	if argument == "OCT":
		month=10
	if argument == "NOV":
		month=11
	if argument == "DEC":
		month=12
	return month
		

	##### PRINT SINGLE CURVE #########################################

def fig1Print(wdata_float,selected_time,local_time, location,location_altitude,zulu_time,filename):
	
	minspeed=min(wdata_float[selected_time][2])
	maxspeed=max(wdata_float[selected_time][2])

	if plt.fignum_exists(1):
		plt.close()
	plot_1 = plt.figure(1, figsize=(20,15))
	figtitle='PLOT 1 - SELECTED DATA - ' + str(filename)
	plot_1.canvas.set_window_title(figtitle)

	##### WIND VELOCITY OVER ALTITUDE  #########################################
	ax1_1=plot_1.add_subplot(1, 2, 1)
	ax1_1.plot(wdata_float[selected_time][2],wdata_float[selected_time][3],'-',label="Wind Velocity [km/h]",color="red")
	ax1_1.plot([minspeed,maxspeed],[location_altitude,location_altitude],'--',label="Airfield Altitude AMSL [m]",color='r',linewidth=4)
	ax1_1.plot(aircraftVelocity(0)[1],aircraftVelocity(0)[0],'--',label="TAS @ 75km/h CAS [km/h]",color='g',linewidth=1)
	ax1_1.plot(aircraftVelocity(0)[2],aircraftVelocity(0)[0],'--',label="TAS @ 85km/h CAS [km/h]",color='b',linewidth=1)
	plt.ylim([0.,20000.])
	plot_1.suptitle(str(location)+ " ("+str(filename) + ")\nLocal Date: " + str(local_time) + "\nZulu-Date: " + str(zulu_time), fontsize=12)
	ax1_1.set_xlabel('Wind Velocity [km/h]',fontsize=10)
	ax1_1.set_ylabel('Altitude [m]',fontsize=10,color="black")
	ax1_1.grid(b=True,which='both',color='k',linestyle='--')
	plt.title('Wind Velocity over Altitude')
	ax1_1.tick_params(axis='y', colors='black')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax1_1.get_legend_handles_labels()
	ax1_1.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)

	
	
	##### WIND DIRECTION OVER ALTITUDE  #########################################
	ax2_1=plot_1.add_subplot(1, 2, 2)
	ax2_1.plot(wdata_float[selected_time][1],wdata_float[selected_time][3],'-',label="Direction [deg]",color="red")
	plt.xlim([0.,360.])
	plt.ylim([0.,20000.])
	ax2_1.set_ylabel('Altitude [m]',fontsize=10)
	ax2_1.set_xlabel('Wind Direction [deg]',fontsize=10,color="black")
	ax2_1.grid(b=True,which='both',color='k',linestyle='--')
	plt.title('Wind Direction over Altitude')
	ax2_1.tick_params(axis='y', colors='black')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax2_1.get_legend_handles_labels()
	ax2_1.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	
	return 0

	
	##### PRINT ALL CURVES ############################################
def allFigsPrint(wdata_float,timedeltas_float,local_time, location, location_altitude,time_shift_to_zulu_time,filename):
	minspeed=1.E+06
	maxspeed=0.
	for i in range(0,len(timedeltas_float)):
		minspeed=min(minspeed,min(wdata_float[i][2]))
		maxspeed=max(maxspeed,max(wdata_float[i][2]))
	plot_2 = plt.figure(2, figsize=(20,15))
	
	figtitle='PLOT 2 - ALL DATA - ' + str(filename)
	plot_2.canvas.set_window_title(figtitle)
	plot_2.suptitle(str(location)+ " ("+str(filename) + str(")\nAirfield Altitude AMSL [m]: ") + str(location_altitude)  + "\nTime Shift relative to Zulu-Time [h]:" + str(time_shift_to_zulu_time), fontsize=12)

	##### WIND VELOCITY  #########################################
	ax41=plot_2.add_subplot(1, 2, 1)
	color_PD=iter(cm.rainbow(np.linspace(0,1,len(timedeltas_float))))
	for i in range(0,len(timedeltas_float)):
		lab=""#"No."+str(i)+"-"+str(local_time[i])
		c=next(color_PD)
		ax41.plot(wdata_float[i][2],wdata_float[i][3],'-',label=lab,color=c)
	ax41.plot([minspeed,maxspeed],[location_altitude,location_altitude],'--',label="Airfield Altitude AMSL [m]",color='r',linewidth=4)
	ax41.plot(aircraftVelocity(0)[1],aircraftVelocity(0)[0],'--',label="TAS @ 75km/h CAS [km/h]",color='g',linewidth=1)
	ax41.plot(aircraftVelocity(0)[2],aircraftVelocity(0)[0],'--',label="TAS @ 85km/h CAS [km/h]",color='b',linewidth=1)
	ax41.set_xlabel('Wind Velocity [km/h]',fontsize=10)
	ax41.set_ylabel('Altitude [m]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Wind Velocity over Altitude')
	lines,labels = ax41.get_legend_handles_labels()
	ax41.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.2, y=0.1)
	plt.ylim([0.,20000.])


	##### WIND DIRECTION  #########################################
	ax42=plot_2.add_subplot(1, 4, 3)
	color_PD=iter(cm.rainbow(np.linspace(0,1,len(timedeltas_float))))
	for i in range(0,len(timedeltas_float)):
		lab=""
		c=next(color_PD)
		ax42.plot(wdata_float[i][1],wdata_float[i][3],'-',label=lab,color=c)
	ax42.set_xlabel('Wind Direction [degree]',fontsize=10)
	ax42.set_ylabel('Altitude [m]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Wind Direction over Altitude')
	lines,labels = ax42.get_legend_handles_labels()
	ax42.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.2, y=0.1)
	plt.xlim([0.,360.])
	plt.ylim([0.,20000.])
	
	##### LEGEND ONLY  #########################################
	ax43=plot_2.add_subplot(1, 4, 4)
	color_PD=iter(cm.rainbow(np.linspace(0,1,len(timedeltas_float))))
	for i in range(0,len(timedeltas_float)):
		lab="#"+str(i)+" "+str(local_time[i])
		c=next(color_PD)
		ax43.plot(0,0,'-',label=lab,color=c)
	
	plt.title('Legend (date refers to local time)')
	plt.axis('off')
	lines,labels = ax43.get_legend_handles_labels()
	ax43.legend(lines, labels, loc=9, prop={'size': 7}, framealpha=0.)

	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	png_name=str(filename)[0:-4]+'.png'
	plt.savefig(png_name)
	
	return 0


	##### PRINT ALL CURVES ############################################
def aircraftVelocity(altitude):
	
	altitudes = range(0,21000,1000)
	
	### Assuming a constant CAS = 75km/h over all altitudes ###
	TAS75=[75,78.7,82.7,87.0,91.7,96.7,102.1,108.0,114.5,121.4,129.,137.4,148.6,160.7,173.8,188.,203.3,219.8,237.6,256.8,277.5]
	
	### Assuming a constant CAS = 85km/h over all altitudes ###
	TAS85=[85,89.2,93.76,98.6,103.9,109.6,115.8,122.4,129.7,137.6,146.2,155.6,168.3,182.,196.8,212.8,230.1,248.7,268.8,290.5,313.8]
	
	#interp_TAS75 = np.interp(altitude,altitudes,TAS75)
	#interp_TAS85 = np.interp(altitude,altitudes,TAS85)
	
	return altitudes, TAS75, TAS85
	


	##### WRITE ALL CURVES TO CSV ############################################
def CSVWriteAllFigs(wdata_float,timedeltas_float,local_time, location, location_altitude,time_shift_to_zulu_time,filename):
	
	### WRITE WIND STRENGTH ###
	csv_filename=str(filename[0:-4])+"wind_strength.csv"
	csvwrite=open(csv_filename,'w') 
	csvwrite.write("Altitude AMSL [m],")
	for i in range(0,len(local_time)):
		csvwrite.write(str(local_time[i])+str(","))
	csvwrite.write("\n")
	for i in range(0,wdata_float.shape[2]): # Loop over Altitude
		csvwrite.write('%.4E' % wdata_float[0][3][i] + str(","))
		for j in range(0,wdata_float.shape[0]): # Loop over Time
			csvwrite.write('%.4E' % wdata_float[j][2][i] + str(","))
		csvwrite.write(str("\n"))	
	csvwrite.close()


	### WRITE WIND DIRECTION ###
	csv_filename=str(filename[0:-4])+"wind_direction.csv"
	csvwrite=open(csv_filename,'w') 
	csvwrite.write("Altitude AMSL [m],")
	for i in range(0,len(local_time)):
		csvwrite.write(str(local_time[i])+str(","))
	csvwrite.write("\n")
	for i in range(0,wdata_float.shape[2]): # Loop over Altitude
		csvwrite.write('%.4E' % wdata_float[0][3][i] + str(","))
		for j in range(0,wdata_float.shape[0]): # Loop over Time
			csvwrite.write('%.1F' % wdata_float[j][1][i] + str(","))
		csvwrite.write(str("\n"))	
	csvwrite.close()	
	
	
	return 0
	
